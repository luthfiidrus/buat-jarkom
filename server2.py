import socket
import time

def doJobs(s):
    split1 = s.split(':')
    if split1[0].lower() == 'fibo':
        return str(doFibo(int(split1[1])))
    elif split1[0].lower() == 'tambah':
        split2 = split1[1].split(',')
        return str(doTambah(int(split2[0]), int(split2[1])))
    elif split1[0].lower() == 'eo':
        split2 = split1[1].split(',')
        return str(doEO(int(split2[0]), int(split2[1])))
    else:
        return 'WIF'

def doTambah(x, y):
    return (x + y)

def doEO(x, y):
    power_cnt = 1
    for i in range (y):
        power_cnt *= x
    
    odd_number_cnt = 0
    even_number_cnt = 0
    for i in str(power_cnt):
        if (int(i) % 2 == 0):
            even_number_cnt += 1
        else:
            odd_number_cnt += 1
    
    res = str(odd_number_cnt) + ";" + str(even_number_cnt)
    return res

def doFibo(x):
    if x <= 0:
        return 0
    if x == 1:
        return 1
    return doFibo(x-1) + doFibo(x-2)

server_port = 2001
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind(('', server_port))
server_socket.listen(5)
print("The server is ready to receive")
c, addr = server_socket.accept()
print("Got connection from", addr)
sentence = c.recv(1024).decode()
list_jobs = sentence.split(" | ")

output = "{:^16}\t{:^16}\t{:^16}\t{:^16}\t{:^16}\n".format("PROCESS", "OUTPUT", "BURST TIME", "WAITING TIME", "TURN AROUND TIME")
list_hasil_jobs = []
burst_time_jobs = []
waiting_time_jobs = []
turn_around_time_jobs = []
for i in range (len(list_jobs)):
    start_time = time.time()
    result = doJobs(list_jobs[i])
    end_time = time.time()
    burst_time = end_time - start_time
    burst_time_jobs.append(burst_time)
    list_hasil_jobs.append(result)
    if i == 0:  
        waiting_time_jobs.append(0)
    else:
        waiting_time_jobs.append(burst_time_jobs[i-1]+waiting_time_jobs[i-1])
    turn_around_time_jobs.append(burst_time_jobs[i]+waiting_time_jobs[i])
    output += "{:^16}\t{:^16}\t{:^16.3f}\t{:^16.3f}\t{:^16.3f}\n".format(i+1, result, burst_time, waiting_time_jobs[i], turn_around_time_jobs[i])

output += "*PS: WIF adalah Wrong Input Format."
c.send(output.encode())
c.close()
